﻿using AppendToMenu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AddProgramToExplorerContextMenuDemo
{
    public partial class Form1 : Form
    {
        Append append = new Append();

        public Form1()
        {
            InitializeComponent();

            lblAdmin.Text = "Running as " + (append.IsUserAdministrator() ? "Admin" : "User");

            //MessageBox.Show("Admin: " + append.IsUserAdministrator().ToString());
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            //PEGAR CAMINHO DO CLIQUE %1
            //"C:\Users\wagne_000\Documents\visual studio 2013\Projects\JustOneInstance\JustOneInstance\bin\Debug\JustOneInstance.exe" "%1%"
            this.append.AddToFolder(this.txtAppName.Text, this.txtCommand.Text, "D:\\Games\\Battlefield 4\\bf4.exe");
            this.append.AddToAnyFile(this.txtAppName.Text, this.txtCommand.Text);
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            this.append.RemoveToFolder(this.txtAppName.Text);
            this.append.RemoveToAnyFile(this.txtAppName.Text);
        }
        
        private void btExists_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Folder: " + this.append.Exists(this.append.ItemFolder).ToString()
                           + "\nAnyFile: " + this.append.Exists(this.append.ItemAnyFile).ToString());
        }
    }
}
