# README #
------
  Adicionar sistema ao menu de contexto do Windows


## Usabilidade ##


* ### Para ser exibido no menu de contexto ###

   >`Append append = new Append()`;


   >Para adicionar precisa que a aplicação esteja rodando em nivel Administrador


   >Para adicionar a aplicaзгo ao menu de contexto apenas das pastas use o metodo abaixo.

   >`this.append.AddToFolder("AppName", "\"C:\\AppName\\\AppName.exe\" \"%1%\"", "C:\\AppName\\AppName.exe");`

   >Para adicionar a aplicaзгo ao menu de contexto a qualquer tipo de arquivo use o metodo abaixo.

   >`this.append.AddToAnyFile("AppName", "\"C:\\AppName\\\AppName.exe\" \"%1%\"", "C:\\AppName\\AppName.exe");`

   >A classe monta o objeto ItemFolder (Ao usar o metodo AddToFolder) para adicionar ao registro o mesmo ocorre com ItemAnyFile (Ao usar o metodo AddToAnyFile) que pode ser acessado publicamente.


* ### Checar se aplicação esta rodando como Administrador ###

   >True se estiver rodando como Admin.		
   >`append.IsUserAdministrator();`