﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;

namespace AppendToMenu
{
    public class Append
    {
        public Item ItemFolder  = new Item("", "Folder", "");
        public Item ItemAnyFile = new Item("", "*", "");

        private void Add(Item item)
        {
            this.CheckSecurity(item);

            RegistryKey regmenu = null;
            RegistryKey regcmd = null;
            try
            {
                regmenu = Registry.ClassesRoot.CreateSubKey(item.GetMenuName());
                if (regmenu != null)
                {
                    regmenu.SetValue("", item.AppName);
                    if(item.IconPath != null)
                        regmenu.SetValue("Icon", item.IconPath);
                }
                regcmd = Registry.ClassesRoot.CreateSubKey(item.GetCommand());
                if (regcmd != null)
                    regcmd.SetValue("", item.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (regmenu != null)
                    regmenu.Close();
                if (regcmd != null)
                    regcmd.Close();
            }
        }

        /// <summary>
        /// Build ItemFolder and add to regestry.
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="value"></param>
        /// <param name="iconPath"></param>
        public void AddToFolder(string appName, string value, string iconPath = null)
        {
            this.Add(this.SetItemValues(this.ItemFolder, appName, value, iconPath));
        }

        /// <summary>
        /// Build ItemAnyFile and add to regestry.
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="value"></param>
        /// <param name="iconPath"></param>
        public void AddToAnyFile(string appName, string value, string iconPath = null)
        {
            this.Add(this.SetItemValues(this.ItemAnyFile, appName, value, iconPath));
        }

        private Item SetItemValues(Item item, string appName, string value, string iconPath = null)
        {
            item.AppName  = appName;
            item.Value    = value;
            item.IconPath = iconPath;

            return item;
        }

        public void RemoveToFolder(string appName)
        {
            this.Remove(this.ItemFolder, appName);
        }

        public void RemoveToAnyFile(string appName)
        {
            this.Remove(this.ItemAnyFile, appName);
        }

        private void Remove(Item item, string appName)
        {
            item.AppName = appName;
            this.Remove(item);
        }

        public void Remove(Item item)
        {
            RegistryKey reg = Registry.ClassesRoot.OpenSubKey(item.GetCommand());
            if (reg != null)
            {
                reg.Close();
                Registry.ClassesRoot.DeleteSubKey(item.GetCommand());
            }
            reg = Registry.ClassesRoot.OpenSubKey(item.GetMenuName());
            if (reg != null)
            {
                reg.Close();
                Registry.ClassesRoot.DeleteSubKey(item.GetMenuName());
            }
        }

        private void CheckSecurity(Item item)
        {
            //check registry permissions
            RegistryPermission regPerm;
            regPerm = new RegistryPermission(RegistryPermissionAccess.Write, "HKEY_CLASSES_ROOT\\" + item.GetMenuName());
            regPerm.AddPathList(RegistryPermissionAccess.Write, "HKEY_CLASSES_ROOT\\" + item.GetCommand());
            regPerm.Demand();
        }

        public bool Exists(Item item)
        {
            RegistryKey reg = Registry.ClassesRoot.OpenSubKey(item.GetCommand());
            if (reg == null)
                return false;
            else if ((string)reg.GetValue(null) != item.Value)
                return false;
            reg = Registry.ClassesRoot.OpenSubKey(item.GetMenuName());
            if (reg == null)
                return false;
            else
                return true;
        }

        public bool IsUserAdministrator()
        {
            bool isAdmin;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
    }
}
