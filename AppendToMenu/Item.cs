﻿namespace AppendToMenu
{
    public class Item
    {
        private string MenuName = "{{TARGET}}\\shell\\{{APPNAME}}";
        private string Command = "{{TARGET}}\\shell\\{{APPNAME}}\\command";

        public string AppName { get; set; }
        public string Target { get; set; }
        public string Value { get; set; }
        public string IconPath { get; set; }

        public Item(string appName, string target, string value)
        {
            this.AppName = appName;
            this.Target = target;
            this.Value = value;
        }

        public string GetMenuName()
        {
            return this.MenuName.Replace("{{APPNAME}}", this.AppName)
                                .Replace("{{TARGET}}", this.Target);
        }

        public string GetCommand()
        {
            return this.Command.Replace("{{APPNAME}}", this.AppName)
                               .Replace("{{TARGET}}", this.Target);
        }
    }
}
